if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config()
}
// Setup basic express server
const express = require('express');
const flash = require('express-flash')
const session = require('express-session')
const app = express();
const path = require('path');
const fs = require('fs')
let server;
if(process.env.NODE_ENV !== 'production') {
  server = require('http').createServer(app);
} else {
  server = require('https').createServer({
    key: fs.readFileSync(
    path.resolve(
       process.env.SSL_DEV_KEY || './ssl/localhost.key'
    )
  ),
  cert: fs.readFileSync(
     path.resolve(
        process.env.SSL_DEV_CRT || './ssl/localhost.crt'
     )
  )
  }, app);
}
const io = require('socket.io')(server);
const expressLayouts = require('express-ejs-layouts')
const mongoose = require('mongoose')
const methodOverride = require('method-override')
const port = process.env.PORT || 3000;

const Users = require('./models/user.js')
const Rooms = require('./models/room.js')
const Messages = require('./models/message.js')


// Load application config/state
//require('./basicstate.js').setup(Users, Rooms);

// Start server

/*server.listen(port, () => {
  console.log('Server listening at port %d', port);
});*/

// Routing for client-side files
mongoose.set('useCreateIndex', true);
app.use(express.static(path.join(__dirname, 'public')));

app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, 'views'))
app.set('layout', 'layouts/layout')
app.use(expressLayouts)
app.use(flash())
app.use(session({
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: false
}))
app.use(methodOverride('_method'))

const indexRouter = require('./routes/index')
const loginRouter = require('./routes/login')(app)
const registerRouter = require('./routes/register').router


app.use(express.urlencoded({
  extended: false
}))

// ROUTING
app.use('/', indexRouter);
app.use('/register', registerRouter);
app.use('/login', loginRouter);

server.listen(port)
////////////////////////////////
//         MongoDB            //
////////////////////////////////

mongoose.connect(process.env.DATABASE_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false
})

const db = mongoose.connection
db.on('error', error => console.error(error))
db.once('open', () => console.error('Connected to Mongoose'))


///////////////////////////////
// Chatroom helper functions //
///////////////////////////////

function messageIdToJSON(msgId) {
  return new Promise((res, reject) => {
    Messages.findById(msgId, (err, doc) =>{
  if(!err){
  Rooms.findById(doc.room, (err, room) => {
    if(!err){
    res( {
    username: doc.sender,
    message: doc.message,
    room: doc.room,
    time: doc.createdAt,
    direct: room.direct
  })
} else {
  reject(err)
}
})
} else {
reject(err)
}
})
})
}

async function fetchMessages(room) {
  const messages = await Promise.all(room.messages.map((msg) => {
    return messageIdToJSON(msg)
  }))
  const r = room.toJSON()
  r.messages = messages
  return r
}

OnlineUsers = []

function sendToRoom(room, event, data) {
  console.log('sending to room: ' + room._id + ' and event: ' + event)
  io.to('room' + room._id).emit(event, data);
}

// function newUser(name) {
//   const user = Users.addUser(name);
//   const rooms = Rooms.getForcedRooms();

//   rooms.forEach(room => {
//     addUserToRoom(user, room);
//   });

//   return user;
// }

function newRoom(name, user, options) {
  const room = Rooms.addRoom(name, options);
  addUserToRoom(user, room);
  return room;
}

function newChannel(name, description, private, user) {
  return newRoom(name, user, {
    description: description,
    private: private
  });
}

function newDirectRoom(user_a, user_b) {
  const room = new Rooms({ //.addRoom(`Direct-${user_a.name}-${user_b.name}`, {
    name: `Direct-${user_a.username}-${user_b.username}`,
    direct: true,
    private: true,
    e2e: true
  });

  return new Promise((res, rej) => {
    room.save(function (err, doc) {
    if (err) {
      return rej(err)
    } else {
      addUserToRoom(user_a, doc);
      addUserToRoom(user_b, doc);
      return res(room);
    }
  })
})
}

function getDirectRoom(user_a, user_b) {
  return new Promise((res, rej) => {
    Rooms.findOne({
    $and: [{
      direct: true
    }, {
      users: {
        $all: [{username: user_a.username, publicKey: user_a.publicKey}, {username: user_b.username, publicKey: user_b.publicKey}]
      }
    }]
  }, async function (err, doc) {
    if (!err) {
      if(doc) {
      const docMessages = await fetchMessages(doc)
      res(docMessages);
    } else {
      const r = await newDirectRoom(user_a, user_b);
      return res(r);
    }
  } else {
    rej(err)
  }
  })
})
}

async function addUserToRoom(user, room) {
  return new Promise((res, rej) => {
    Users.findByIdAndUpdate(user._id, {
    $push: {
      rooms: room._id
    }
  }, (err, usr) => {//user.addSubscription(room);
    Rooms.findByIdAndUpdate(room._id, {
      $push: {
        users: {username: user.username, publicKey: user.publicKey}
      }
    },
    {"new": true}, (err, updatedRoom) => {
      console.log("THE ROOM IS")
      console.log(updatedRoom)
      sendToRoom(updatedRoom, 'update_user', {
        room: room._id,
        username: user.username,
        action: 'added',
        users: updatedRoom.users
      });
      return res(updatedRoom)
    })    
  })  
})
}



function removeUserFromRoom(user, room) {
  Users.findByIdAndUpdate(user._id, {
    $pull: {
      rooms: mongoose.Types.ObjectId(room._id)
    }
  }, () => {}) //user.removeSubscription(room);
  Rooms.findByIdAndUpdate(room._id, {
    $pull: {
      users: {"username": user.username}
    }
  },
  {"new": true}, (err, room) => {
    sendToRoom(room, 'update_user', {
      room: room._id,
      username: user.username,
      action: 'removed',
      users: room.users
    });
  }) //room.removeMember(user);
}

function addMessageToRoom(roomId, username, msg) {
  const message = new Messages({
    message: msg.message,
    sender: msg.username,
    room: roomId,
    receiver: msg.to
  })

  message.save((err, doc) => {
    if (!err) {
      Rooms.findByIdAndUpdate(roomId, {
        $push: {
          messages: doc._id
        }
      }, function (err, room) {
        if (!err) {
          sendToRoom(room, 'new message', {
            username: username,
            message: msg.message,
            room: msg.room,
            time: doc.createdAt,
            direct: room.direct,
            receiver: msg.to
          });
        } else {
          console.log(err)
        }
      })
    } else {
      console.log(err)
    }
  })
}

function setUserActiveState(socket, username, state) {
  Users.findOne({
    username: username
  }).exec((err, res) => {
    if (!err) {
      res.state = state;

      socket.broadcast.emit('user_state_change', {
        username: username,
        active: state
      });
    }
  })
}

///////////////////////////
// IO connection handler //
///////////////////////////

const socketmap = {};

io.on('connection', (socket) => {
  let loggedIn = false;
  let username = false;

  ///////////////////////
  // incomming message //
  ///////////////////////

  socket.on('new message', (msg) => {
    if (loggedIn) {
      console.log(msg);
      addMessageToRoom(msg.room, username, msg);
    }
  });

  /////////////////////////////
  // Request message from Id //
  /////////////////////////////

  socket.on('request_message', req => {
    return null
  })

  /////////////////////////////
  // request for direct room //
  /////////////////////////////


  socket.on('request_direct_room', req => {
    if (loggedIn) {
      Users.find({
        username: {$in: [req.to, username]}
      }, async (err, docs) =>{
        if (docs.length == 2) {
          const a = docs[0]
          const b = docs[1]
          const room = await getDirectRoom(a, b);
          console.log(room)
          const roomCID = 'room' + room._id;
          socket.join(roomCID);
          if (socketmap[new Buffer(a.username).toString('base64')])
            socketmap[new Buffer(a.username).toString('base64')].join(roomCID);
  
          socket.emit('update_room', {
            room: room,
            moveto: true
          });
        } 
      })
    }
  });

  socket.on('add_channel', req => {
    if (loggedIn) {
      Users.findOne({
        username: username
      }, (err, user) => {
        if (!err) {
          console.log(req);
          const room = new Rooms({
            name: req.name,
            description: req.description,
            private: req.private,
            users: {username: username, publicKey: user.publicKey},
            forceMembership: false,
            direct: false,
            e2e: req.e2e
          }) //newChannel(req.name, req.description, req.private, user);
          room.save(function (err, room) {
            if (err) {
              console.log(err)
              socket.emit('room_error', {
                error: err
              })
            } else {
              const roomCID = 'room' + room._id;
              socket.join(roomCID);

              user.rooms.push(room._id);
              user.save();

              socket.emit('update_room', {
                room: room,
                moveto: true
              });

              if (!room.private) {
                Rooms.find({
                  $and: [{
                    private: false
                  }, {
                    direct: false
                  }]
                }, function (err, docs) {
                  if (!err) {
                    socket.broadcast.emit('update_public_channels', {
                      publicChannels: docs
                    });
                  } else {
                    console.log(err)
                    socket.emit('room_error', {
                      error: err
                    })
                  }
                }) //getRooms().filter(r => !r.direct && !r.private);
              }
            }
          })
        }
      }) //.getUser(username);

    }
  });

  socket.on('join_channel', req => {
    if (loggedIn) {
      Users.findOne({
        username: username
      }, (err, user) => {
        console.log("USER JOINED")
         Rooms.findById(req.id, async (err, room) => {
          if (!err) {
            if (!room.direct && !room.private) {
              const updatedRoom = await addUserToRoom(user, room);
              
              const roomCID = 'room' + room._id;
              socket.join(roomCID);
              const r = await fetchMessages(updatedRoom)
            socket.emit('update_room', {
              room: r,
              moveto: true
            });
          }
        } else {
          console.log(err)
        }
      })
      
    })
    }
  });


  socket.on('add_user_to_channel', req => {
    if (loggedIn) {
      Users.findOne({
        username: req.user
      }, (err, user) => {
        
       //.getUser(req.user);
      Rooms.findById(req.channel, async (err, rm) => {
        if (!rm.direct) {
          const room = await addUserToRoom(user, rm);
  
          const roomCID = 'room' + room._id;
          socketmap[new Buffer(user.username).toString('base64')].join(roomCID);
  
          socketmap[new Buffer(user.username).toString('base64')].emit('update_room', {
            room: room,
            moveto: false
          });
        }
      }) //getRoom(req.channel)
    })
    }
  });

  socket.on('leave_channel', req => {
    if (loggedIn) {
      console.log(req)
      Users.findOne({
        username: username
      }, (err, user) => {
        if(!err){
          Rooms.findById(req.id, (err, room) => {
            if(!err){
              if (!room.direct && !room.forceMembership) {
                console.log("REMOVING USER")
              removeUserFromRoom(user, room);
       
              const roomCID = 'room' + room._id;
              socket.leave(roomCID);
      
              socket.emit('remove_room', {
                room: room._id
              });
            }
          } else {
            console.log(err)
          }
          }) //getRoom(req.id)
    
          
        }
      }) //.getUser(username);
      
    }
  });

  ///////////////
  // user join //
  ///////////////

  socket.on('publicKey', (data) =>{
    console.log(data)
    Users.findOneAndUpdate({
      username: data.username
    }, {publicKey: data.pub}, function(err, user){})
  })

  socket.on('privateKey', (data) =>{
    console.log(data)
    Users.findOneAndUpdate({
      username: data.username
    }, {privateKey: data.prv}, function(err, user){})
  })

  socket.on('join', (p_username) => {
    if (loggedIn)
      return;

    username = p_username;
    loggedIn = true;
    socketmap[new Buffer(username).toString('base64')] = socket;

    Users.findOne({
      username: username
    }, function (err, user) {

      OnlineUsers.push(user)

      user.rooms.map(s => {
        socket.join('room' + s);
      });

      Rooms.find({}, async function (err, rms) {
        const rooms = await Promise.all(rms.map(fetchMessages))
        
        const publicChannels = rooms.filter(r => !r.direct && !r.private);
        const subscribedChannels = rooms.filter(r => user.rooms.includes(r._id) )
        socket.emit('login', {
          userKeys: {private: user.privateKey, public: user.publicKey},
          users: OnlineUsers.map(u => ({
            username: u.username,
            active: u.active
          })),
          rooms: subscribedChannels,
          publicChannels: publicChannels
        });
        setUserActiveState(socket, username, true);
    })
    })
  });

  /////////////////
  // disconnects //
  /////////////////

  socket.on('disconnect', () => {
    if (loggedIn)
      setUserActiveState(socket, username, false);
  });

  ////////////////
  // reconnects //
  ////////////////

  socket.on('reconnect', () => {
    if (loggedIn)
      setUserActiveState(socket, username, true);
  });

});