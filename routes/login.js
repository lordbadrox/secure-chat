const express = require('express')
const router = express.Router()
const passport = require('passport')
const initPassport = require('../passport-config')
const users = require('./register').users

function initRouter(app) {
    //Passport
    initPassport(passport)

    app.use(passport.initialize())
    app.use(passport.session())

    // Login page
    router.get('/', checkLoggedOut, (req, res) => {
        res.render('login', {
            isLoggedIn: req.isAuthenticated()
        })
    })

    router.post('/', passport.authenticate('local', {
         successRedirect: '/',
         failureRedirect: '/login',
         failureFlash: true
     }))
    
    return router
}

function checkLoggedOut(req, res, next) {
    if (req.isAuthenticated()) {
        return res.redirect('/')
    }
    next()
  }

module.exports = initRouter