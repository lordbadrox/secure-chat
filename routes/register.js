const express = require('express')
const router = express.Router()
const bcrypt = require('bcrypt')
const User = require('../models/user')

const users = []

// Login page
router.get('/', checkLoggedOut, (req, res) => {
    res.render('register', {
        isLoggedIn: req.isAuthenticated(),
        error: ''
    })
})

router.post('/', checkLoggedOut, (req, res) =>{
    // try {
    //     const hashedPassword = await bcrypt.hash(req.body.password, 10)
    //     const user = {id: Date.now().toString(), name: req.body.name, email: req.body.email, password: hashedPassword}
    //     users.push(user)
    //     res.redirect('/login')
    // } catch(e){
    //     res.redirect('/register')
    // }
    User.register(new User({username: req.body.name, email: req.body.email}), req.body.password, function(err, user) { 
        if (err) { 
          console.log(err);
          return res.render('register', {
            isLoggedIn: req.isAuthenticated(),
            error: err.message
        })
        }else{ 
          return res.redirect('/login')
        } 
      }); 
})

function checkLoggedOut(req, res, next) {
    if (req.isAuthenticated()) {
        return res.redirect('/')
    }
    next()
  }

module.exports = {router: router, users: users}