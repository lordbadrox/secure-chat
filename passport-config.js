const LocalStrategy =  require('passport-local').Strategy
const bcrypt = require('bcrypt')
const User = require('./models/user'); 
  
//const LocalStrategy = require('passport-local').Strategy; 


function initialize(passport) {
    // const authUser = (email, password, done) => {
    //     const user = User.findOne({email: email}, async function(err, user) {
    //         if(user == null){
    //             return done(null, false, {message: 'No user found with the email'})
    //         }
    
    //         try {
    //             if (await bcrypt.compare(password, user.password)) {
    //                 return done(null, user)
    //             } else {
    //                 return done(null, false, {message: 'Password incorrect'})
    //             }
    //         } catch (e) {
    //             return done(e)
    //         }
    //     })
    // }
    passport.use(new LocalStrategy(User.authenticate()));
    passport.serializeUser(User.serializeUser()); 
    passport.deserializeUser(User.deserializeUser()); 
    //passport.use(new localStrat({usernameField: 'email'}, authUser))

    //passport.serializeUser((user, done) => done(null, user.id))
    //passport.deserializeUser((id, done) => done(null, getUserById(id)))
}

module.exports = initialize