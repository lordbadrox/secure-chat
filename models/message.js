var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var MsgSchema = new mongoose.Schema({
    message:{
        type: String,
        required: true
    },
    sender:{
        type: String,
        required: true
    },
    date: {
        type: Date
    },
    room: {
        type: ObjectId,
        ref:'Room',
        required: true
    },
    receiver: {
        type: String
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('Message', MsgSchema);