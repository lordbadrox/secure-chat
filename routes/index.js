const express = require('express')
const router = express.Router()
const io = require('socket.io')
const $ = require('jquery')
const jsdom = require( "jsdom" );

// Login page
router.get('/', checkAuth, (req, res) => {
    res.render('index',{
      isLoggedIn: req.isAuthenticated(),
      userName: passValue(req.user.username)
  })
})

router.delete('/', checkAuth, (req, res) => {
  req.logOut()
  res.redirect('/login')
})

function passValue(value) {
  return 'JSON.parse(Base64.decode("' + new Buffer.from(JSON.stringify(value)).toString('base64') + '"))'
}

function checkAuth (req, res, next) {
    if(req.isAuthenticated()) {
      return next()
    }
    res.redirect('/login')
  }

module.exports = router