var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var ObjectId = mongoose.Schema.Types.ObjectId;

var RoomSchema = new mongoose.Schema({
    name: {
        type: String,
        lowercase: true,
        unique: true
    },
    description: String,
    users: [],
    messages: [{
        type: String
    }],
    private:{
        type: Boolean
    },
    forceMembership:{
        type: Boolean
    },
    direct:{
        type: Boolean
    },
    e2e: {
        type:Boolean
    }
}, {
    timestamps: true
});

RoomSchema.plugin(uniqueValidator, {
    message: 'Room name is already taken.'
});

module.exports = mongoose.model('Room', RoomSchema);