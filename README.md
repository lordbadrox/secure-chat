
# Secure chat

A simple secure chat

## How to use

```
$ cd secure-chat
$ npm install
$ npm start
```

And point your browser to `http://localhost:3000`. Optionally, specify
a port by supplying the `PORT` env variable.